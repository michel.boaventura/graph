#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <stdlib.h>
#include <stdint.h>
#include "khash.h"

KHASH_MAP_INIT_INT(32, int32_t)
KHASH_MAP_INIT_INT(g, khash_t(32) *)

#define graph_num_vertices(graph) kh_size(graph->graph)
#define graph_num_edges(graph) graph->edges

typedef struct graph_t {
  uint32_t edges;
  khash_t(g) *graph;
  khash_t(32) *in_degree;
} graph_t;

typedef void (*graph_foreach_fn)(uint32_t s, uint32_t t, void *data);

graph_t * graph_new();
void graph_free(graph_t *graph);
void graph_add_edge(graph_t *graph, uint32_t s, uint32_t t);
uint32_t graph_out_degree(graph_t *graph, uint32_t s);
uint32_t graph_in_degree(graph_t *graph, uint32_t s);
uint8_t graph_has_edge(graph_t *graph, uint32_t s, uint32_t t);
void graph_foreach_t(graph_t *graph, uint32_t s, graph_foreach_fn, void *data);
void graph_foreach(graph_t *graph, graph_foreach_fn, void *data);

#endif
