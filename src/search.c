#include "search.h"

/* create an array of n ints initialized to SEARCH_INFO_NULL */
static int *
create_empty_array(int n) {
  int *a;
  int i;

  a = malloc(sizeof(*a) * n);

  for(i = 0; i < n; i++)
    a[i] = SEARCH_INFO_NULL;

  return a;
}

/* allocate and initialize search results structure */
/* you need to do this before passing it to dfs or bfs */
struct search_info *
search_info_create(graph_t *graph) {
  struct search_info *s;
  int n;

  s = malloc(sizeof(*s));

  s->graph = graph;
  s->reached = 0;

  n = graph_num_vertices(graph);

  s->preorder = create_empty_array(n);
  s->time = create_empty_array(n);
  s->parent = create_empty_array(n);
  s->depth = create_empty_array(n);

  return s;
}

/* free search_info data---does NOT free graph pointer */
void
search_info_destroy(struct search_info *s) {
  free(s->depth);
  free(s->parent);
  free(s->time);
  free(s->preorder);
  free(s);
}

static void
push_edge(graph_t *graph, int u, int v, void *data) {
  (void) graph;
  struct queue *q;

  q = data;

  q->e[q->top].u = u;
  q->e[q->top].v = v;
  q->top++;
}

/* this rather horrible function implements dfs if use_queue == 0 */
/* and bfs if use_queue == 1 */
static void
generic_search(struct search_info *r, int root, int use_queue) {
  /* queue/stack */
  struct queue q;

  /* edge we are working on */
  struct edge cur;

  /* start with empty q */
  /* we need one space per edge */
  /* plus one for the fake (root, root) edge */
  q.e = malloc(sizeof(*q.e) * (graph_num_edges(r->graph) + 1));

  q.bottom = q.top = 0;

  /* push the root */
  push_edge(r->graph, root, root, &q);

  /* while q.e not empty */
  while(q.bottom < q.top) {
    if(use_queue) {
      cur = q.e[q.bottom++];
    } else {
      cur = q.e[--q.top];
    }

    /* did we visit sink already? */
    if(r->parent[cur.v] != SEARCH_INFO_NULL) continue;

    /* no */
    r->parent[cur.v] = cur.u;
    r->time[cur.v] = r->reached;
    r->preorder[r->reached++] = cur.v;
    if(cur.u == cur.v) {
      /* we could avoid this if we were certain SEARCH_INFO_NULL */
      /* would never be anything but -1 */
      r->depth[cur.v] = 0;
    } else {
      r->depth[cur.v] = r->depth[cur.u] + 1;
    }

    /* push all outgoing edges */
    /*TODO graph_foreach(r->graph, cur.v, push_edge, &q);*/
  }

  free(q.e);
}

void
dfs(struct search_info *results, int root) {
  generic_search(results, root, 0);
}

void
bfs(struct search_info *results, int root) {
  generic_search(results, root, 1);
}
