#include "main.h"

typedef struct stat_t {
  int total_paths, total_nodes;
  double *betweenness, *closeness;
} stat_t;

stat_t *
stat_new(int total_nodes) {
  stat_t *stat;

  stat = malloc(sizeof(stat_t));
  stat->total_nodes  = total_nodes;
  stat->total_paths  = 0;
  stat->betweenness  = calloc(total_nodes, sizeof(double));
  stat->closeness    = calloc(total_nodes, sizeof(double));

  return stat;
}

void
stat_print(stat_t *stat) {
  int i;

  printf("Betweenness: \n");
  for(i = 0; i < stat->total_nodes; i++)
    printf("%d -> %f\n", i, stat->betweenness[i]);

  printf("Closeness: \n");
  for(i = 0; i < stat->total_nodes; i++)
    printf("%d -> %f\n", i, stat->closeness[i]);
}

void
stat_free(stat_t *stat) {
  free(stat->betweenness);
  free(stat->closeness);
  free(stat);
}

void
stat_finalize(stat_t *stat) {
  int i;

  for(i = 0; i < stat->total_nodes; i++) {
    stat->closeness[i]   /= stat->total_nodes;
    stat->betweenness[i] /= stat->total_paths;
  }

}

void
stat_update(stat_t *stat, struct search_info *s, int current_node) {
  int tmp, i;

  for(i = 0; i < stat->total_nodes; i++) {
    stat->closeness[current_node] += s->depth[i];
    tmp = s->parent[i];
    while(tmp != s->parent[tmp]) {
      stat->betweenness[tmp]++;
      stat->total_paths++;
      tmp = s->parent[tmp];
    }
  }
}

int
main(int argc, char *argv[]) {
  int i, size;
  graph_t *graph;
  struct search_info *s;

  if(argc != 3) {
    printf("Usage: ./%s graph_size graph_file seeds_file\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  size = atoi(argv[1]);

  graph = graph_new(size);

  for(i = 0; i < size; i++) {
    s = search_info_create(graph);
    bfs(s, i);
    search_info_destroy(s);
  }

  graph_free(graph);

  return 0;
}
