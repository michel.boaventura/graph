#include "graph.h"

graph_t *
graph_new() {
  graph_t *graph;

  graph = malloc(sizeof(graph_t));

  graph->edges     = 0;
  graph->graph     = kh_init(g);
  graph->in_degree = kh_init(32);

  return graph;
}

void
graph_free(graph_t *graph) {
  khint_t i;

  for(i = kh_begin(graph->graph); i != kh_end(graph->graph); ++i)
    if(kh_exist(graph->graph, i))
      kh_destroy(32, kh_val(graph->graph, i));

  kh_destroy(g, graph->graph);
  kh_destroy(32, graph->in_degree);

  free(graph);
}

uint32_t
graph_in_degree(graph_t *graph, uint32_t s) {
  khint_t k;

  k = kh_get(32, graph->in_degree, s);

  return k == kh_end(graph->in_degree) ? 0 : kh_val(graph->in_degree, k);
}

uint32_t
graph_out_degree(graph_t *graph, uint32_t s) {
  khint_t k;

  k = kh_get(g, graph->graph, s);

  return k == kh_end(graph->graph) ? 0 : kh_size(kh_val(graph->graph, k));
}

static void
graph_add_to_hashes(graph_t *graph, uint32_t s) {
  int absent;
  khint_t k;

  k = kh_put(g, graph->graph, s, &absent);

  if(absent) {
    kh_key(graph->graph, k) = s;
    kh_val(graph->graph, k) = kh_init(32);

    /* Adding s to in degree hash */
    k = kh_put(32, graph->in_degree, s, &absent);
    kh_key(graph->in_degree, k) = s;
    kh_val(graph->in_degree, k) = 0;
  }
}

void
graph_add_edge(graph_t *graph, uint32_t s, uint32_t t) {
  int absent;
  khash_t(32) *neighbours;
  khint_t k;

  graph_add_to_hashes(graph, s);
  graph_add_to_hashes(graph, t);

  /* Getting s' neighbours */
  k = kh_get(g, graph->graph, s);
  neighbours = kh_val(graph->graph, k);

  /* Adding t to neighbours */
  k = kh_put(32, neighbours, t, &absent);

  if(absent) {
    kh_key(neighbours, k) = t;
    kh_val(neighbours, k) = 1;
    graph->edges++;

    /* Incrementing t's in degree */
    k = kh_get(32, graph->in_degree, t);
    kh_val(graph->in_degree, k)++;
  }
}

void
graph_foreach_t(graph_t *graph, uint32_t s, graph_foreach_fn fn, void *data) {
  khint_t k;
  uint32_t key;
  uint8_t val;
  (void) val;

  khash_t(32) *neighbours;

  k = kh_get(g, graph->graph, s);

  if(k == kh_end(graph->graph))
    return;

  neighbours = kh_val(graph->graph, k);

  kh_foreach(neighbours, key, val, fn(s, key, data));
}

uint8_t
graph_has_edge(graph_t *graph, uint32_t s, uint32_t t) {
  khint_t k;
  khash_t(32) *neighbours;

  k = kh_get(g, graph->graph, s);

  if(k == kh_end(graph->graph))
    return 0;

  neighbours = kh_val(graph->graph, k);

  k = kh_get(32, neighbours, t);

  return k != kh_end(neighbours);
}
