#include "test_search.h"

int test_size = 199137;

///* never call this */
//static void
//match_sink(graph_t *graph, int source, int sink, void *data) {
//  (void) graph;
//  (void) source;
//  assert_int_equal(data && sink, *((int *) data));
//}

void
test_dfs() {
  graph_t *graph;
  int i;
  int j;
  struct search_info *s;

  graph = graph_new(test_size);

  /* put all nodes on a long path */
  /* and all nodes as children of 0 */
  /* except for node n-1 */
  for(i = 0; i < test_size - 2; i++) {
    graph_add_edge(graph, i, i);        /* red herring */
    graph_add_edge(graph, i, i+1);      /* for DFS */
  }

  /* quick check to make sure we put in the edges we thought we did */
  assert_int_equal(graph_vertex_count(graph), test_size);
  assert_int_equal(graph_edge_count(graph), (test_size-2)*2);

  /* do dfs starting from 0 */
  s = search_info_create(graph);
  dfs(s, 0);

  /* what did we learn? */
  assert_int_equal(s->reached, test_size - 1);
  for(i = 0; i < test_size - 1; i++) {
    if(i == 0) {
      assert_int_equal(s->parent[i], i);
    } else {
      assert_int_equal(s->parent[i], i-1);
    }
    assert_int_equal(s->preorder[i], i);
    assert_int_equal(s->time[i], i);
    assert_int_equal(s->depth[i], i);
  }

  /* now do more dfs from n-1 */
  dfs(s, test_size-1);

  assert_int_equal(s->reached, test_size);
  for(i = 0; i < test_size; i++) {
    if(i == 0 || i == test_size - 1) {
      assert_int_equal(s->parent[i], i);
    } else {
      assert_int_equal(s->parent[i], i-1);
    }
    assert_int_equal(s->preorder[i], i);
    assert_int_equal(s->time[i], i);
    if(i == test_size - 1) {
      assert_int_equal(s->depth[i], 0);
    } else {
      assert_int_equal(s->depth[i], i);
    }
  }

  search_info_destroy(s);
  graph_free(graph);
}

void
test_bfs() {
  graph_t *graph;
  int i;
  int j;
  struct search_info *s;

  /* now try bfs */
  graph = graph_new(test_size);

  for(i = 1; i < test_size - 1; i++) {
    graph_add_edge(graph, i, i);
    graph_add_edge(graph, 0, i);
  }

  graph_add_edge(graph, 1, test_size - 1);

  assert_int_equal(graph_edge_count(graph), 2*(test_size - 2) + 1);

  s = search_info_create(graph);
  bfs(s, 0);

  /* what did we learn? */
  assert_int_equal(s->reached, test_size);
  for(i = 0; i < test_size - 1; i++) {
    assert_int_equal(s->parent[i], 0);
    assert_int_equal(s->preorder[s->time[i]], i);
    if(i == 0) {
      assert_int_equal(s->depth[i], 0);
    } else {
      assert_int_equal(s->depth[i], 1);
    }
  }

  /* n-1 should be hit last */
  assert_int_equal(s->parent[test_size - 1], 1);
  assert_int_equal(s->preorder[test_size - 1], test_size - 1);
  assert_int_equal(s->time[test_size - 1], test_size - 1);
  assert_int_equal(s->depth[test_size - 1], 2);

  search_info_destroy(s);
  graph_free(graph);
}

int
main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_bfs),
    cmocka_unit_test(test_dfs),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}

