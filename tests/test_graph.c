#include "test_graph.h"

#define SIZE 10

static void
graph_fn(uint32_t source, uint32_t target, void *data) {
  int **edges = data;
  edges[source][target] = 1;
}

void
test_graph() {
  graph_t *graph = graph_new(SIZE);
  int i, j, **edges;
  int correct_edges[SIZE][SIZE] = {
    {0, 1, 1, 1, 1, 1, 0, 0, 0, 1},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 1},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  };

  edges = malloc(sizeof(int *) * SIZE);

  for(i = 0; i < SIZE; i ++)
    edges[i] = calloc(sizeof(int), SIZE);

  graph_add_edge(graph, 0, 1);
  graph_add_edge(graph, 0, 2);
  graph_add_edge(graph, 0, 3);
  graph_add_edge(graph, 0, 4);
  graph_add_edge(graph, 0, 5);
  graph_add_edge(graph, 0, 9);

  graph_add_edge(graph, 5, 6);
  graph_add_edge(graph, 5, 7);
  graph_add_edge(graph, 5, 8);
  graph_add_edge(graph, 5, 9);

  assert_int_equal(graph_num_vertices(graph), 10);
  assert_int_equal(graph_num_edges(graph), 10);

  assert_int_equal(graph_out_degree(graph, 0), 6);
  assert_int_equal(graph_in_degree(graph, 9), 2);

  graph_foreach_t(graph, 0, graph_fn, edges);
  graph_foreach_t(graph, 5, graph_fn, edges);

  for(i = 0; i < SIZE; i++) {
    for(j = 0; j < SIZE; j++) {
      assert_int_equal(correct_edges[i][j], graph_has_edge(graph, i, j));
      assert_int_equal(correct_edges[i][j], edges[i][j]);
    }
    free(edges[i]);
  }
  free(edges);

  graph_free(graph);
}

int
main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_graph),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
